(* --------------------------------- ProBsa --------------------------------- *)
From probsa.util Require Export notation.

(* ---------------------------------- Main ---------------------------------- *)
Lemma bigsum_distr :
  forall (t1 t2 : nat) (f : nat -> R) (c : R),
    ∑_{t1 <= i <= t2} c * f i = c * ∑_{t1 <= i <= t2} f i.
Proof.
  intros; have [LE|LT] := leqP t1 t2; last first.
  { by rewrite !big_geq; try assumption; nra. }
  have EQ: exists δ, (t2 = t1 + δ)%nat; [by exists (t2 - t1)%nat; rewrite subnKC | ].
  elim: EQ => [δ EQ]; subst t2; clear LE.
  induction δ.
  - by rewrite addn0 !big_nat1.
  - rewrite !addnS big_nat_recr //= ?IHδ; clear IHδ; last by rewrite ltnW //= ltnS leq_addr.
    rewrite !big_nat_recr //=; try rewrite ltnW //= ltnS; try by rewrite leq_addr.
    by nra.
Qed.

Lemma bigsum_add :
  forall (t1 t2 : nat) (f g : nat -> R),
    ∑_{t1 <= i <= t2} (f i + g i) = ∑_{t1 <= i <= t2} f i + ∑_{t1 <= i <= t2} g i.
Proof.
  intros; have [LE|LT] := leqP t1 t2; last first.
  { by rewrite !big_geq; try assumption; nra. }
  have EQ: exists δ, (t2 = t1 + δ)%nat; [by exists (t2 - t1)%nat; rewrite subnKC | ].
  elim: EQ => [δ EQ]; subst t2; clear LE.
  induction δ.
  - by rewrite addn0 !big_nat1.
  - rewrite !addnS big_nat_recr //= ?IHδ; clear IHδ; last by rewrite ltnW //= ltnS leq_addr.
    rewrite !big_nat_recr //=; try rewrite ltnW //= ltnS; try by rewrite leq_addr.
    by nra.
Qed.
