(* --------------------------- Reals and SSReflect -------------------------- *)
Require Export Reals Psatz.
From mathcomp Require Export ssreflect ssrnat ssrbool seq eqtype fintype.

(* --------------------------------- Prosa ---------------------------------- *)
From prosa.util Require Import list.

(* --------------------------------- ProBsa --------------------------------- *)
From probsa.util Require Export boolp.
Local Open Scope nat_scope.

(* ---------------------------------- Main ---------------------------------- *)
Definition min_option (xs : seq nat) : option nat :=
  match xs with
  | [::] => None
  | h::tl => Some (foldl minn h tl)
  end.

Definition min_default (d : nat) (xs : seq nat) : nat := foldl minn d xs.


(** We prove that any monotone function [f : nat -> bool] falls into one of
    the following cases:
    (1) [f n] is false for any [n],
    (2) there exists [t0] such that [forall t, t > t0 <-> f t],
    (3) or [f n] is always true.
    We note that this lemma uses the classical excluded middle axiom
    [forall P : Prop, P \/ ~ P]. *)
Lemma swithing_point_of_monotone_function :
  forall (f : nat -> bool),
    (forall t1 t2, t1 <= t2 -> f t1 -> f t2) ->
    (forall t, ~ f t) \/ (exists t0, forall t, t > t0 <-> f t) \/ (forall t, f t).
Proof.
  intros f MON.
  destruct (Classical_Prop.classic (exists t, f t)) as [EX|NEX].
  - right; destruct EX as [t0 Ft].
    induction t0 as [ | t].
    + by right => t; eapply MON; last by apply Ft.
    + destruct (f t) eqn:Ft0; first by apply IHt.
      clear IHt; left; exists t; intros t0.
      split; intros.
      * by eapply MON; last by apply Ft.
      * rewrite leqNgt; apply/negP; move => LE; apply ltnSE in LE.
        by apply MON in LE => //=; rewrite LE in Ft0.
  - by rewrite -forallNE in NEX; left.
Qed.


(** Function that changes a type of a job [s] by mapping [s : Job] to
    [s : Job \ {j}]. *)
Definition to_fintype {Job : finType} (s j : Job) (NEQ: s != j) :
  Finite.sort (seq_sub_finType (T:=Job) (rem (T:=Job) j (enum Job))).
Proof.
  econstructor; instantiate (1 := s).
  apply in_neq_impl_rem_in => //.
  by rewrite mem_enum unfold_in.
Defined.
