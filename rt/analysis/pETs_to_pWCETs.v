(* --------------------------------- ProBsa --------------------------------- *)
From probsa.rt.model Require Export task events.

(* ---------------------------------- Main ---------------------------------- *)
(** In this file, we define the response-time monotonic transformation
    presented in the paper "What really is pWCET? A Rigorous Axiomatic 
    Definition of pWCET" by Bozhko et al. (RTSS'23). *)

(** * Transformation *)
(** As discussed in the paper, there are potentially many different
    response-time monotonic transformations (e.g., one can imagine a
    trivial transformation that simply maps probabilistic job costs to
    WCETs). However, we are interested in one specific transformation
    that decouples job-cost dependencies, thus turning job costs into
    IID random variables.

    In this section, we define a response-time monotonic
    transformation of the probability space that ensures the validity
    of axiomatic pWCET (defined in [rt.model.axiomatic_pWCET]). *)
Section Transformation.

  (** Consider any type of tasks with a notion of pWCET. *)
  Context {Task : TaskType}
          {pWCET_pmf : ProbWCET Task}.

  (** Consider a finite set of jobs associated with the tasks. *)
  Context {Job : finType}
          {job_task : JobTask Job Task}.

  (** ** System *)
  (** For the sake of construction, we introduce a concept of a
      "system" [S], which is a record type containing a sample space
      [Ω_of S], a measure [μ_of S], arrival times [𝓐_of S], and
      execution costs [𝓒_of S]. *)
  Record system := {
      Ω_of : countType;
      μ_of : measure Ω_of;
      𝓐_of : JobArrivalRV Job Ω_of μ_of;
      𝓒_of : JobCostRV Job Ω_of μ_of;
    }.

  (** ** pWCET to Job Cost Random Variable *)
  (** Given a pWCET, we can construct a new random variable describing
      a job's cost with the same distribution as its task's pWCET's
      probability mass function (PMF) [pWCET_pmf]. Note that this
      procedure involves defining a new sample space (the set of
      natural numbers), a new measure (that coincides with the pWCET's
      PMF), and a new job-cost function.

      The definition might seem a bit involved, so one might think
      about this function as follows. The pWCET of a task assigns
      a probability to each natural number. We can take these
      probabilities and construct a new probability space where [Ω =
      nat] and [μ c = pWCET_pmf c]. Next, we can define a random variable
      on this space as [fun c => Some c]; all this variable does is
      convert a natural number [c] to [Some c], which is Coq's way
      of constructing the type [𝕎 ∪ ⊥]. Note that [fun c => Some c] is a
      pET on [Ω = nat]. Indeed, its type is [Ω → 𝕎 ∪ ⊥].

      Next, we use this random variable to construct pETs on the
      initial sample space [Ω_of S]. *)
  Definition pWCET_to_RVpWCET (tsk : Task) :
    {Ω : countType & {μ : (measure) Ω & JobCostRV Job Ω μ}} :=
    (** Destruct [pWCET_pmf] into its constituent parts [pWCET],
        [nonneg], and [sum1]. *)
    match pWCET_pmf with
    | Build_ProbWCET pWCET nonneg sum1 =>
        (* [Ω := ℕ] *)
        existT [countType of nat]
               (* [μ c := pWCET_pmf c] *)
               (existT (mkDistrib _ (pWCET tsk) (nonneg tsk) (sum1 tsk))
                       (* [job_cost c := c] *)
                       (fun j => mkRvar _ (fun w => Some w)))
    end.

  (** ** Replace pET of a Job *)
  (** Suppose we are given a system [S = (Ω, μ, 𝓐, 𝓒)] and a job
      [j]. To construct a new system [S'] with [j]'s pET replaced
      with its task's pWCET, we define [(Ω', μ', 𝓐', 𝓒')] as follows.  Let [(Ωj,
      μj, Cj) := pWCET_to_RVpWCET(job_task j)]. Then, the new system
      is defined as follows: *)
  Definition replace_job_pET  (j : Job) (S : system) : system :=
    match S with
    | Build_system Ω μ 𝓐 𝓒 =>
        match pWCET_to_RVpWCET (job_task j) with
        | existT Ωj (existT μj Cj) =>
            (** 1. [Ω' := Ω × Ωj] — the new sample space is a cross
                product of [Ω] and [Ωj]. *)
            let Ω' := [countType of (Ω * Ωj)%type] in
            (** 2. [μ' := fun (ω, ωj) => (μ ω) × (μj ωj)] — the new
                measure on [Ω'] is defined as a product of [μ] and
                [μj]. *)
            let μ' := distrib_prod μ μj in
            (** 3. [𝓐'(jo) := 𝓐(jo)] — arrival times remain the same. *)
            let 𝓐' := fun jo => mkRvar μ' (fun '(ω, _) => 𝓐 jo ω) in
            (** 4. [𝓒'(jo) := fun (ω, ωj) => if (j == jo) then (Cj jo ωj) else (𝓒 jo ω)] —
                given a job [jo: jo != j], its cost is defined on [Ω] and does
                not change, for job [j] the cost is redefined as [Cj j ωj]. *)
            let 𝓒' := fun jo => mkRvar μ' (fun '(ω, ωj) =>
                                          if (j == jo) then Cj j ωj else 𝓒 jo ω) in
            Build_system Ω' μ' 𝓐' 𝓒'
        end
    end.

  (** ** Replace pETs of a Set of Jobs *)
  (** We can iteratively apply the function [replace_job_pET] for each job in a
      given set of jobs simply via [foldr]. *)
  Definition replace_all_jobs_pETs (Ωμ : system) (js : seq Job) : system :=
    foldr replace_job_pET Ωμ js.

  (** ** Replace pETs of All Jobs *)
  (** To replace _all_ job pETs, we simply need to repeat the procedure for
      all jobs (i.e., [index_enum Job]). *)
  Definition replace_all_pETs :=
    replace_all_jobs_pETs^~(index_enum Job).

End Transformation.
