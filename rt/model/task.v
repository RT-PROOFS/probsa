(* --------------------------------- Prosa ---------------------------------- *)
From prosa.model Require Export task.concept.

(* --------------------------------- ProBsa --------------------------------- *)
From probsa.probability Require Export nrvar.

(* ---------------------------------- Main ---------------------------------- *)
(** * PMF of pWCET *)
(** In this file, we define pWCET's probability mass function. The
    typeclass contains three elements: (1) a function called
    [pWCET_pmf] that takes a [Task] and a [work] as input and returns
    a value in [R] that is supposed to denote the "probability" of a
    task to take a certain value, (2) a proof term that [pWCET_pmf] is
    non negative for any task and any work value, and (3) a proof term
    that [pWCET_pmf] sums up to 1.

    It is important to note that even though [R] represents the set of
    real numbers, in the context of probability theory, probabilities
    are always represented by values between [0] and [1]. Therefore,
    the [pWCET_pmf] function actually returns a value within the
    closed interval [[0,1]], representing the probability of a
    given [Task] completing after consuming [work] time units of
    processor service. *)
Class ProbWCET (Task : TaskType) := {
    pWCET_pmf : Task -> (work -> R);
    pWCET_nonnegative : forall (tsk : Task), ∀ (w : work), pWCET_pmf tsk w >= 0;
    pWCET_sum1 : forall (tsk : Task), is_series (countable_sum (pWCET_pmf tsk)) 1
  }.

(* ------------------------------ Definitions ------------------------------- *)
(** * CDF of pWCET *)
Section CDFPWCET.

  Context {Ω} (μ : measure Ω).

  Context {Task : TaskType}
          {pWCET : ProbWCET Task}.

  (** Next, we define a more common form of pWCET as a CDF. Note that
      since [pWCET_pmf] is not an actual random variable, we cannot
      re-use the existing definition of CDF. Hence, we simply define
      [pWCET_cdf tsk w0] as a sum of [pWCET_pmf tsk w] of all [w <=
      w0]. *)
  Definition pWCET_cdf (tsk : Task) (w0 : nat) :=
    ∑[∞]_{w <- nat} if (w <= w0)%nat then pWCET_pmf tsk w else 0.

End CDFPWCET.
