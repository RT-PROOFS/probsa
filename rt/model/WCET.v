(* --------------------------------- Prosa ---------------------------------- *)
Require Export prosa.model.task.concept.

(* --------------------------------- ProBsa --------------------------------- *)
From probsa.rt.behavior Require Export job.

(* ---------------------------------- Main ---------------------------------- *)
(** * Worst-Case Execution Time *)
Section WCET.

  (** Consider a probability space [(Ω, μ)], ... *)
  Context {Ω} {μ : measure Ω}.

  (** ... any type of tasks with costs defined by a function
      [task_cost], ... *)
  Context {Task : TaskType}
          {task_cost : TaskCost Task}.

  (** ... and any type of jobs associated with these tasks. Assume
      that the jobs have probabilistic costs defined by [job_cost]. *)
  Context {Job : JobType}.
  Context {job_cost : JobCostRV Job Ω μ}
          {job_task : JobTask Job Task}.

(** We say that [task_cost] is a WCET iff for any job [j] and its task
    [job_task j] the two conditions hold:

    (i) The job's cost is less than or equal to the task's cost in all
    evolutions [ω ∈ Ω]. The statement uses the [odflt0] function to
    safely extract the value of [job_cost j], or fallback to zero if
    [job_cost j] is [⊥] for the given [ω].

    (ii) There exists an evolution [ω] such that [job_cost j] exactly
    matches the task's cost in [ω]. *)
  Definition task_cost_is_WCET :=
    forall j,
      (forall ω, odflt0 (job_cost j) ω <= task_cost (job_task j))%nat
      /\ (exists ω, odflt0 (job_cost j) ω = task_cost (job_task j)).

End WCET.
