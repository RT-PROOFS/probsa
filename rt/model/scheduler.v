(* --------------------------------- Prosa ---------------------------------- *)
From prosa.model Require Import processor.ideal.

(* -------------------------------- ProBsa ---------------------------------  *)
From probsa.rt Require Import behavior.response_time.

(* ------------------------------ Definitions ------------------------------- *)
(** * Scheduler *)
Section SchedulerHardcoded.

  Variable horizon : option instant.

  (** Consider any type of job. *)
  Context {Job : JobType}.

  (** We define a scheduler with hardcoded arrivals and costs as a
      function that receives two functions -- one describing job
      arrivals [𝗔 : Job -> option instant] and another describing job
      costs [𝗖 : Job -> option work] -- and returns a schedule (which
      is a function that maps a time instant to a processor state at
      this time instant). Note that the functions do not depend on [ω]
      and, hence, are fixed. *)
  (** The motivation behind such a scheduler is to encode _a
      scheduling algorithm_ that makes its decisions based only on the
      information provided by inputs from [𝗔] and [𝗖]. *)
  Definition scheduler𝗔𝗖 : Type :=
    ((* 𝗔 *) Job -> option instant) -> ((* 𝗖 *) Job -> option work)
    -> instant -> ideal.processor_state Job.

  (** The following definitions just mirror the existing definitions.
      The only difference is that the proposed definitions use the
      scheduler instead of a generic schedule. *)

  Definition service𝗔𝗖 : Type :=
    ((* 𝗔 *) Job -> option instant) -> ((* 𝗖 *) Job -> option work)
    -> Job -> instant -> work.

  Definition completed_by𝗔𝗖 : Type :=
    ((* 𝗔 *) Job -> option instant) -> ((* 𝗖 *) Job -> option work)
    -> Job -> instant -> bool.

  Definition response_time𝗔𝗖 : Type :=
    ((* 𝗔 *) Job -> option instant) -> ((* 𝗖 *) Job -> option work)
    -> Job -> etime.

  Definition scheduler𝗔𝗖_to_service𝗔𝗖 (ζ : scheduler𝗔𝗖) : service𝗔𝗖 :=
    fun 𝗔 𝗖 j t => service (ζ 𝗔 𝗖) j t.

  Definition scheduler𝗔𝗖_to_completed𝗔𝗖 (ζ : scheduler𝗔𝗖) : completed_by𝗔𝗖 :=
    fun 𝗔 𝗖 j t => 𝗖 j ⟨<=⟩ service (ζ 𝗔 𝗖) j t.

  (** Given a scheduler, we define a function that maps [𝗔 : Job ->
      option instant] and [𝗖 : Job -> option work] to a function that
      can compute the response time of any given job. *)
  Section ResponseTimeFromScheduler.

    Variable ζ : scheduler𝗔𝗖.
    Let completed_by := scheduler𝗔𝗖_to_completed𝗔𝗖 ζ.

    Definition completed𝗔𝗖_is_dec :
      forall 𝗔 𝗖 (j : Job) (compl_time : instant),
        completed_by 𝗔 𝗖 j compl_time ∨ ¬ completed_by 𝗔 𝗖 j compl_time.
    Proof.
      by intros * => //=; elim (completed_by 𝗔 𝗖 j compl_time); auto.
    Defined.

    Definition min_completion_time 𝗔 𝗖 (j : Job) :=
      LPO_min (fun compl_time => completed_by 𝗔 𝗖 j compl_time) (completed𝗔𝗖_is_dec 𝗔 𝗖 j).

    Definition scheduler𝗔𝗖_to_rt𝗔𝗖 : response_time𝗔𝗖 :=
      fun 𝗔 𝗖 j =>
        match 𝗔 j with
        | None => Undef
        | Some ar =>
            match min_completion_time 𝗔 𝗖 j, horizon with
            | inright _, _ => Infty
            | inleft (exist compl_time _), None => Fin (compl_time - ar)
            | inleft (exist compl_time _), Some h =>
                if (compl_time >= h)%nat then Infty else Fin (compl_time - ar)
            end
        end.

  End ResponseTimeFromScheduler.

End SchedulerHardcoded.

(** * Scheduler to Schedule *)
Section Schedule.

  Context {Ω} {μ : measure Ω}.
  Context {Job : JobType}
          {job_arrival : JobArrivalRV Job Ω μ}
          {job_cost : JobCostRV Job Ω μ}.

  (** If we are given a scheduler [sched], then a
      probabilistic schedule is defined as follows: *)

  (** [λ (ω : Ω) (t : instant),
         sched (compute_arrivals ω) (compute_costs ω) t]. *)

  (** Here, [compute_arrivals] ([compute_costs]) is a function that
      maps [ω ∈ Ω] to a function with fixed arrivals (costs). *)

  (** Similarly to [arival_sequence], [pr_schedule] is an arrow type
      ([instant -> PState]), Coq cannot automatically derive the right
      type. Therefore, we need to steer Coq's coercion and type
      systems towards the right type via the annotation [B :=
      Equality.clone _ _].*)
  Definition compute_pr_schedule (sched : @scheduler𝗔𝗖 Job) : pr_schedule μ (ideal.processor_state Job) :=
    mkRvar _ (B := Equality.clone _ _)
           (fun ω t =>
              sched (compute_arrivals ω) (compute_costs ω) t
           ).

End Schedule.

(** * RT-Monotonic Scheduler *)
Section RTMonotonicScheduler.

  Context {Ω} {μ : measure Ω}.
  Context {Job : JobType}
          {job_arrival : JobArrivalRV Job Ω μ}
          {job_cost : JobCostRV Job Ω μ}.

  Variable horizon : option instant.

  (** Consider a scheduling algorithm [ζ]. *)
  Variable ζ : @scheduler𝗔𝗖 Job.

  (** For convenience, let [𝓡] denote an algorithm that maps
      job arrivals, job costs, and a job to its response time. *)
  Let 𝓡 := scheduler𝗔𝗖_to_rt𝗔𝗖 horizon ζ.

  (** We let [sched] denote a schedule computed via [ζ]. *)
  Let sched := @compute_pr_schedule Ω μ _ _ _ ζ.

  (** We prove that the response time of a job computed in [sched] is
      equal to the response time of the same job computed via [𝓡] if
      run with job arrivals equal to [compute_arrivals ω] and job
      costs equal to [compute_costs ω]. *)
  Lemma valid_𝓡 :
    forall (j : Job) (ω : Ω),
      response_time sched horizon j ω = 𝓡 (compute_arrivals ω) (compute_costs ω) j.
  Proof.
    intros *.
    rewrite /𝓡 /response_time /scheduler𝗔𝗖_to_rt𝗔𝗖 => //=.
    rewrite [compute_arrivals ω j]/compute_arrivals.
    elim JA: (job_arrival j ω) => [a| ]; last by done.
    have EQU :
      forall i,
        pr_completed_by sched j i ω
        = scheduler𝗔𝗖_to_completed𝗔𝗖 ζ (compute_arrivals ω) (compute_costs ω) j i.
    { by intros t; rewrite /scheduler𝗔𝗖_to_completed𝗔𝗖 /pr_completed_by /sched => //=. }
    elim (response_time.min_completion_time sched ω j) => [[compl1 [COMPL1 MIN1]]|NCOMLP1].
    all: elim (min_completion_time ζ (compute_arrivals ω) (compute_costs ω) j)
       => [[compl2 [COMPL2 MIN2]]|NCOMPL2].
    all: try done.
    { have ->: compl1 = compl2; last by done.
      apply/eqP; rewrite eqn_leq; apply/negbNE/negP.
      rewrite negb_and -!ltnNge => /orP [LT|LT].
      - by eapply MIN1; [apply/ltP; apply LT | apply COMPL2].
      - by eapply MIN2; [apply/ltP; apply LT | apply COMPL1].
    }
    { by exfalso; apply: (NCOMPL2 compl1); rewrite -EQU. }
    { by exfalso; eapply NCOMLP1; erewrite EQU; eapply COMPL2. }
  Qed.

  (** To define RT-monotonicity, we define a function that updates a
      given vector with a new cost. *)
  Definition update (𝗖 : Job -> option work) (jo : Job) (c : option work) : (Job -> option work) :=
      fun j => if j == jo then c else 𝗖 j.

  (** A scheduler [ζ] is response-time monotonic if, given vectors of
      fixed arrival times [𝗔] and job costs [𝗖], an update of the cost
      of any job [j_update] from [c1] to [c2 : c1 ⟨≤⟩ c2] cannot cause
      a decrease in response time of any job [j]. *)
  Definition rt_monotonic_scheduler :=
    forall 𝗔 𝗖 (j_update j : Job), ∀ (r : instant) (c1 c2 : option nat),
      is_true (c1 ⟨<=⟩ c2) →
      exceeds (𝓡 𝗔 (update 𝗖 j_update c1) j) r →
      exceeds (𝓡 𝗔 (update 𝗖 j_update c2) j) r.

End RTMonotonicScheduler.

Opaque scheduler𝗔𝗖_to_rt𝗔𝗖.
