<div align="center">
![ProBsa Logo](probsa-logo.jpg){width=35%}
</div>

# ProBsa: Axiomatic pWCET in Coq

---

This repository contains the formal proof for the paper:

- Sergey Bozhko, Filip Marković, Georg von der Brüggen, and Björn B. Brandenburg, “What Really is pWCET? A Rigorous Axiomatic Proposal”, *Proceedings of the 44th IEEE Real-Time Systems Symposium (RTSS 2023)*, IEEE, December 2023.


## Build Instructions


To generate the `html` documentation, run the following commands:
```
$ git clone -b main https://gitlab.mpi-sws.org/RT-PROOFS/probsa.git probsa
$ cd probsa
$ docker run --rm -v $(pwd):/probsa -w /probsa gkerfimf/probsa:latest bash -c './create_makefile.sh && make vacuum && make gallinahtml'
```

<details>
  <summary> Explanation of the commands </summary>

```
git clone -b main https://gitlab.mpi-sws.org/RT-PROOFS/probsa.git probsa
^^^^^^^^^ ^^^^^^^ ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ^^^^^^
│         │      │                                                └────────── Clone the repo into a directory called `probsa`
│         │      └──────────────────────────────────────────────────────────────────────────── Url of the `ProBsa` repository
│         └─────────────────────────────────────────── Clone the `main` branch, which is the branch where all the fun happens
└─────────────────────────────────────────────────────────────────────────────────────────────── Clone the repository via Git

cd probsa
^^^^^^^^^
└────────────────────────────────────────────────────────────────────────── Navigate to the folder containing the source code


docker run --rm -v $(pwd):/probsa -w /probsa gkerfimf/probsa:latest bash -c './create_makefile.sh && make vacuum && make html'
^^^^^^^^^^ ^^^^ ^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ^^^^^^^^^^^^^^^^^^^^^^ ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
│          │    │                            │                      └─────────── Compile Coq files and generate HTML documents
│          │    │                            └─────────────────────────── Docker image that has all the necessary dependencies
│          │    └──────────────────────────────────────────────── Set up the container, so it has access to the `ProBsa` files
│          └────────────────────────────────────────────────────────────────── Delete the container after the task is complete
└───────────────────────────────────────────────────────────────────────────────────────── Run the build in a docker-container
```

</details>

For detailed instructions and an explanation of the codebase, see the artifact evaluation guidelines in the branch `AE-RTSS2023`.


## Use of Classical Logic

The development of ProBsa relies on several assumptions of classical logic. A few notable examples are as follows.
1. The law of excluded middle is used to perform case analyses on the possibility of various events. One example is the step when `ℙ[... ∩ ξi]` is transformed to conditional probability `ℙ[... | ξi]`, where `ξi` is an event corresponding to a fixed arrival sequence. Our Coq proof does a case analysis on whether there exists an evolution `ω` that satisfies `ξi`. Such a case analysis is impossible in the intuitionistic Coq's logic since it does not provide a (terminating) algorithm to find such an element.
2. The proof-irrelevance is used to construct some of the Ω-partitions (e.g., for partition on arrival sequences `ξ`).
3. Functional extensionality is used in the main proof (Theorem 1 and Theorem 2) to rewrite between distinct random variables that are equal for all their arguments.
4. Finally, the content of `boolp` was used to derive an equality on probabilistic schedules.

### Soundness

ProBsa essentially assumes only three axioms that are compatible together  (`functional_extensionality`, `constructive_indefinite_description`, and `classic`).
The axioms are compatible because functional extensionality, predicate extensionality, and indefinite description imply classic (very bottom of this [file](https://www.chargueraud.org/viewcoq.php?sFile=softs/tlc/src/UseClassic.v)). So ProBsa just assumes `classic` instead of predicate extensionality.

Next, 
* `classic` implies `proof_irrelevance`,
* `classic` implies `eq_rect_eq` (Streicher's K) which in turn implies `JMeq_eq` (see this [discussion on SO](https://cstheory.stackexchange.com/questions/42527/an-axiom-for-john-majors-equality#comment98809_42527)),
* `classic` implies the axioms in `boolp` (because they are the same), and
* `sig_not_dec` and `sig_forall_dec` should follow from `classic`.


