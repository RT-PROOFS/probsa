#!/bin/bash 

# options passed to `find` for locating relevant source files
FIND_OPTS=( -name '*.v' ! -name '*#*' ! -path './.git/*')

FIND_OPTS+=( -print )

# Compile all relevant *.v files
coq_makefile -f _CoqProject \
    $(find ./rt/behavior "${FIND_OPTS[@]}") \
    $(find ./rt/model "${FIND_OPTS[@]}") \
    $(find ./rt/analysis "${FIND_OPTS[@]}") \
    $(find ./probability "${FIND_OPTS[@]}") \
    $(find ./util "${FIND_OPTS[@]}") \
    -o Makefile

# Patch HTML target to switch out color, and 
# so that it parses comments and has links to ssreflect.
# Also include Makefile.coqdocjs for 'htmlpretty' documentation target.
patch -s < scripts/Makefile.patch